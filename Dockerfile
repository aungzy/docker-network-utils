FROM debian

# Install essential tools for network tests
RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install procps curl telnet traceroute vim net-tools dnsutils

CMD [ "/bin/bash" ]